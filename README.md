# Smolpxl Remote

A small Rust executable designed to allow Smolpxl games to talk to each other.

This application runs as a server, allowing two or more browser-based games
to connect to it.  Once the games are connected, they can send messages to
each other.

Communication is done using WebSockets.

## Build and run

[Install Rust](https://www.rust-lang.org/tools/install), then:

```bash
cargo run
```

## Running integration tests

The integration tests run in a web browser, since that is our targeted use
case.

Run the program with default settings and open a browser to
http://localhost:4433/static/index.html

```bash
RUST_LOG=debug cargo run &
firefox http://localhost:4433/static/index.html
```

## Protocol

The examples below assume the server is running on localhost port 4433, which
should work if you started it using `cargo run`.

### Starting a new game

To establish a new game, send the exact string `new` to the server.

In JavaScript, this looks like:

```javascript
let game = new WebSocket("ws://127.0.0.1:4433/");
game.onopen = function() {
    game.send("new");
};
game.onmessage = function(event) {
    console.log(`Received '${event.data}' on game`);
};
```

If the connection worked, the server will respond telling you the game id:

```
{"id":"foobar", "secret":"tellnoone"}
```

You should share the `id` with anyone you want to connect to your game, but
never share the `secret` since it allows others to kick you out and take over
as the owner of the game (see "Taking over a game" below).

### Connecting to an existing game

To connect to an existing game, send the string `connect $ID`, but replace
`$ID` with the ID that was returned from the `new` command (above).

In JavaScript, this looks like:

```javascript
let sat = new WebSocket("ws://127.0.0.1:4433/");
sat.onopen = function() {
    sat.send(`connect foobar`);
};
sat.onmessage = function(event) {
    if (event.data == '{"status":"connected"}') {
        console.log(`Satellite connected OK.`);
    }
};
```

### Sending game info

You may connect several "satellites" to a game by using the `connect $ID`
command.

When you send text to the original WebSocket created using `new`, it will be
received by all the connected satellites.

When you send text to a satellite WebSocket, it will be received by the
original game only.

### Taking over a game

If you created a game but your connection drops, you may want to reclaim it.

To do that, send the string `take $ID $SECRET` but replace `$ID` and `$SECRET`
with the values that were returned from the `new` command.

In JavaScript, this looks like:

```javascript
let newgame = new WebSocket("ws://127.0.0.1:4433/");
newgame.onopen = function() {
    newgame.send(`take foobar tellnoone`);
};
newgame.onmessage = function(event) {
    console.log(`Received '${event.data}' on newgame`);
};
```

## Deployment

(Obviously you can't follow this section unless you have SSH access to the
deployment server.)

We build a fully-static release executable to deploy, and copy it to the
target machine.  We don't actually restart the application or replace the
running executable - you are expected to do that yourself.

First time, install some tools:

```bash
make setup
```

(For non Debian-based OSs, read the Makefile and do similar setup.)

Then to build a release version and copy to production:

```bash
make deploy-aarch64
```

Follow the instructions to attach to the running tmux session, stop the
application, replace the executable with the `.new` one, and restart.

## License and credits

Copyright 2021 Andy Balaam and contributors, released under the
[AGPLv3 license](LICENSE) or later.

For testing, the source repo contains a copy of
[Jasmine](https://jasmine.github.io), which is released under the
[MIT license](static/jasmine/MIT.LICENSE).  (This is not included in the
compiled releases.)

## Code of conduct

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md).  By participating in this
project you agree to abide by its terms.

[![Contributor Covenant](images/contributor-covenant-v2.0-adopted-ff69b4.svg)](code_of_conduct.md)
