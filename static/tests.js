'use strict';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 5000;
// const url = "ws://127.0.0.1:4433/";
// Alternatively, to test the production instance:
const url = "wss://smolpxl.uk.to:4433/";
const POLICY_VIOLATION = 1008;


class Game {
    constructor(take_id, take_secret) {
        this.oncreated = () => {};
        this.onmessage = () => {};
        this.onerror = (evt) => {fail(evt);}
        this.state = "init";
        this.sock = new WebSocket(url);
        this.sock.onopen = (evt) => {
            this.state = "open";
            if (take_id) {
                this.sock.send(`take ${take_id} ${take_secret}`);
            } else {
                this.sock.send('new');
            }
        };
        this.sock.onmessage = (evt) => {
            if (this.state === "open") {
                let data = JSON.parse(evt.data);
                if (data.id && data.id.match(/[0-9a-zA-Z]+/)) {
                    this.state = "created";
                    this.id = data.id;
                    this.secret = data.secret;
                    this.oncreated();
                } else {
                    this.onerror(evt.data);
                }
            } else if (this.state === "created") {
                this.onmessage(evt.data);
            } else {
                fail(`Message received, but we are in state ${this.state}`);
            }
        };
        this.sock.onclose = (evt) => {
            this.state = "closed";
        };
        this.sock.onerror = (evt) => {
            this.onerror(evt);
        };
    }

    send(msg) {
        if (this.state === "created") {
            this.sock.send(msg);
        } else {
            fail(`Sending message, but we are in state ${this.state}`);
        }
    }
}


class Satellite {
    constructor(id) {
        this.onconnected = () => {};
        this.onmessage = () => {};
        this.state = "init";
        this.sock = new WebSocket(url);
        this.sock.onopen = (evt) => {
            this.state = "open";
            this.sock.send(`connect ${id}`);
        };
        this.sock.onmessage = (evt) => {
            if (this.state === "open") {
                expect(evt.data).toBe('{"connected":"ok"}');
                this.state = "connected";
                this.onconnected(id);
            } else if (this.state == "connected") {
                this.onmessage(evt.data);
            } else {
                fail(`Message received, but we are in state ${this.state}`);
            }
        };
        this.sock.onclose = (evt) => {
            this.state = "closed";
        };
        this.sock.onerror = (evt) => {
            fail(evt);
        };
    }

    send(msg) {
        if (this.state === "connected") {
            this.sock.send(msg);
        } else {
            fail(`Sending message, but we are in state ${this.state}`);
        }
    }
}


describe("Server", function() {

    it("should error and close if sent JSON before new", function(done) {
        let sock = new WebSocket(url);
        sock.onopen = function() {
            sock.send('{"some":"data"}');
        };
        sock.onmessage = function(evt) {
            expect(evt.data).toBe(
                '{"error":"Expecting \'new\', \'connect ID\', ' +
                'or \'take ID SECRET\'."}'
            );
            done();
        };
        sock.onerror = function(evt) {
            fail(evt);
        };
    });

    it("should not crash if sent short string before new", function(done) {
        let sock = new WebSocket(url);
        sock.onopen = function() {
            sock.send('unsup');
        };
        sock.onmessage = function(evt) {
            expect(evt.data).toBe(
                '{"error":"Expecting \'new\', \'connect ID\', ' +
                'or \'take ID SECRET\'."}'
            );
            done();
        };
        sock.onerror = function(evt) {
            fail(evt);
        };
    });

    it("should send ID when sent new", function(done) {
        let sock = new WebSocket(url);
        sock.onopen = function() {
            sock.send('new');
        };
        sock.onmessage = function(evt) {
            let data = JSON.parse(evt.data);
            expect(data.id).toMatch(/[0-9a-zA-Z]+/);
            done()
        };
    });

    it("should accept new with extra whitespace", function(done) {
        let sock = new WebSocket(url);
        sock.onopen = function() {
            sock.send(' new\n');
        };
        sock.onmessage = function(evt) {
            let data = JSON.parse(evt.data);
            expect(data.id).toMatch(/[0-9a-zA-Z]+/);
            done()
        };
    });

    it("should error and close if we connect on a bad ID", function(done) {
        let game = new Game();
        game.oncreated = () => {
            let sat = new WebSocket(url);
            sat.onopen = function() {
                sat.send(`connect INCORRECT${game.id}`);
            };
            sat.onmessage = function(evt) {
                expect(evt.data).toBe(
                    '{"error":"connect failed - no such game"}'
                );
                done();
                // Note: we should create a sat.onclose too, and
                // confirm that we close the connection too, but that
                // was sometimes failing because the socket had been
                // aborted.
            };
            sat.onerror = function(evt) {
                fail(evt);
            };
        };
    });

    it("should send OK if we connect to an existing game", function(done) {
        let game = new Game();
        game.oncreated = () => {
            let sat = new WebSocket(url);
            sat.onopen = function() {
                sat.send(`connect ${game.id}`);
            };
            sat.onmessage = function(evt) {
                expect(evt.data).toBe('{"connected":"ok"}');
                done();
            };
        };
    });

    it("should send OK if we connect several times", function(done) {
        let game = new Game();
        game.oncreated = () => {
            let sat1 = new WebSocket(url);
            sat1.onopen = function() {
                sat1.send(`connect ${game.id}`);
            };
            sat1.onmessage = function(evt) {
                expect(evt.data).toBe('{"connected":"ok"}');
                let sat2 = new WebSocket(url);
                sat2.onopen = function() {
                    sat2.send(`connect ${game.id}`);
                };
                sat2.onmessage = function(evt) {
                    expect(evt.data).toBe('{"connected":"ok"}');
                    done();
                };
            };
        };
    });

    it("should pass messages from game to satellites", function(done) {
        const msg = '{"message":1}';
        let received1 = false;
        let received2 = false;
        let game = new Game();
        game.oncreated = () => {
            let sat1 = new Satellite(game.id);
            sat1.onconnected = () => {
                let sat2 = new Satellite(game.id);
                sat2.onconnected = () => {
                    game.send(msg);
                };
                sat2.onmessage = (data) => {
                    expect(data).toBe(msg);
                    received2 = true;
                    if (received1 && received2) {
                        done();
                    }
                };
            };
            sat1.onmessage = (data) => {
                expect(data).toBe(msg);
                received1 = true;
                if (received1 && received2) {
                    done();
                }
            };
        };
    });

    it("should pass messages from satellites to game", function(done) {
        const msg1 = '{"message":1}';
        const msg2 = '{"message":2}';
        let received1 = false;
        let received2 = false;
        let game = new Game();
        game.oncreated = () => {
            let sat1 = new Satellite(game.id);
            sat1.onconnected = () => {
                let sat2 = new Satellite(game.id);
                sat2.onconnected = () => {
                    sat1.send(msg1);
                    sat2.send(msg2);
                };
            }
        };
        game.onmessage = (msg) => {
            if (msg === msg1) {
                received1 = true;
            } else if (msg === msg2) {
                received2 = true;
            }
            if (received1 && received2) {
                done();
            }
        }
    });

    it("should pass multiple messages in both directions", function(done) {
        const msg_from_game1 = '{"from_game":1}';
        const msg_from_game2 = '{"from_game":2}';
        const msg_from_satA1 = '{"from_satA":1}';
        const msg_from_satA2 = '{"from_satA":2}';
        const msg_from_satB1 = '{"from_satB":1}';
        const msg_from_satB2 = '{"from_satB":2}';
        let received_from_gameA1 = false;
        let received_from_gameA2 = false;
        let received_from_gameB1 = false;
        let received_from_gameB2 = false;
        let received_from_satA1 = false;
        let received_from_satA2 = false;
        let received_from_satB1 = false;
        let received_from_satB2 = false;

        function checkDone() {
            if (
                received_from_gameA1 &&
                received_from_gameA2 &&
                received_from_gameB1 &&
                received_from_gameB2 &&
                received_from_satA1 &&
                received_from_satA2 &&
                received_from_satB1 &&
                received_from_satB2
            ) {
                done();
            }
        }

        let game = new Game();
        game.oncreated = () => {
            let satA = new Satellite(game.id);
            satA.onconnected = () => {
                let satB = new Satellite(game.id);
                satB.onconnected = () => {
                    satA.send(msg_from_satA1);
                    satB.send(msg_from_satB1);
                    game.send(msg_from_game1);
                    satA.send(msg_from_satA2);
                    satB.send(msg_from_satB2);
                    game.send(msg_from_game2);
                };
                satB.onmessage = (msg) => {
                    if (msg === msg_from_game1) {
                        received_from_gameB1 = true;
                    } else if (msg === msg_from_game2) {
                        received_from_gameB2 = true;
                    } else {
                        fail(`Unexpected message ${msg}`);
                    }
                    checkDone();
                };
            };
            satA.onmessage = (msg) => {
                if (msg === msg_from_game1) {
                    received_from_gameA1 = true;
                } else if (msg === msg_from_game2) {
                    received_from_gameA2 = true;
                } else {
                    fail(`Unexpected message ${msg}`);
                }
                checkDone();
            };
        };
        game.onmessage = (msg) => {
            if (msg === msg_from_satA1) {
                received_from_satA1 = true;
            } else if (msg === msg_from_satA2) {
                received_from_satA2 = true;
            } else if (msg === msg_from_satB1) {
                received_from_satB1 = true;
            } else if (msg === msg_from_satB2) {
                received_from_satB2 = true;
            } else {
                fail(`Unexpected message ${msg}`);
            }
            checkDone();
        }
    });

    it("should give a different ID to each new game", function(done) {
        let game1 = new Game();
        game1.oncreated = () => {
            let game2 = new Game();
            game2.oncreated = () => {
                expect(game1.id).not.toBe(game2.id);
                done()
            };
        };
    });

    it("should keep messages from separate games separate", function(done) {
        let rec1 = [];
        let rec2 = [];


        function checkDone() {
            rec1.sort();
            rec2.sort();
            if (
                (
                    JSON.stringify(rec1) ==
                       '[' +
                           '"game1->sats",' +
                           '"game1->sats",' +
                           '"sat1A->game1",' +
                           '"sat1B->game1"' +
                       ']'
                ) && (
                    JSON.stringify(rec2) ==
                       '[' +
                           '"game2->sats",' +
                           '"game2->sats",' +
                           '"sat2A->game2",' +
                           '"sat2B->game2"' +
                       ']'
                )
            ) {
                done();
            }
        }

        let game1 = new Game();
        let game2 = new Game();
        game1.oncreated = () => {
            let sat1A = new Satellite(game1.id);
            sat1A.onconnected = () => {
                sat1A.send("sat1A->game1");
                let sat1B = new Satellite(game1.id);
                sat1B.onconnected = () => {
                    sat1B.send("sat1B->game1");
                    game1.send("game1->sats");
                };
                sat1B.onmessage = (msg) => {
                    rec1.push(msg);
                    checkDone();
                };
            };
            sat1A.onmessage = (msg) => {
                rec1.push(msg);
                checkDone();
            };
        };
        game1.onmessage = (msg) => {
            rec1.push(msg);
            checkDone();
        };
        game2.oncreated = () => {
            let sat2A = new Satellite(game2.id);
            sat2A.onconnected = () => {
                sat2A.send("sat2A->game2");
                let sat2B = new Satellite(game2.id);
                sat2B.onconnected = () => {
                    sat2B.send("sat2B->game2");
                    game2.send("game2->sats");
                };
                sat2B.onmessage = (msg) => {
                    rec2.push(msg);
                    checkDone();
                };
            };
            sat2A.onmessage = (msg) => {
                rec2.push(msg);
                checkDone();
            };
        };
        game2.onmessage = (msg) => {
            rec2.push(msg);
            checkDone();
        };
    });

    it("should return a secret when we create a game", function(done) {
        let game = new Game();
        game.oncreated = () => {
            expect(game.secret).not.toBeFalsy();
            done()
        };
    });

    it("should fail to replace a game with wrong secret", function(done) {
        let game = new Game();
        game.oncreated = () => {
            let sat = new Satellite(game.id);
            sat.onconnected = () => {
                let taker = new Game(game.id, "wrong!");
                taker.oncreated = () => {
                    fail("Created taker with wrong secret!");
                    done();
                };
                taker.onerror = () => {
                    // Messages still flow.  First from game to sat
                    game.send("Message 1 g->s");
                };
            };
            sat.onmessage = (msg) => {
                expect(msg).toBe("Message 1 g->s");
                // Then back again
                sat.send("Message 2 s->g");
            };
        };
        game.onmessage = (msg) => {
            expect(msg).toBe("Message 2 s->g");
            // The taker failed, and we sent a message in both directions
            done();
        };
    });

    it("should be able to replace a game", function(done) {
        let game = new Game();
        game.oncreated = () => {
            let sat = new Satellite(game.id);
            sat.onconnected = () => {
                let taker = new Game(game.id, game.secret);
                taker.oncreated = () => {
                    // We took the game - send messages through game
                    // game and ourselves - only ours should get through.
                    game.send("Should not arrive");
                    taker.send("Should arrive");
                };
                taker.onmessage = (msg) => {
                    expect(msg).toBe("And reply");
                    // We sent and received through taker!
                    done();
                };
            };
            sat.onmessage = (msg) => {
                expect(msg).toBe("Should arrive");
                sat.send("And reply");
            };
        };
        game.onmessage = (msg) => {
            fail("No message should get back to game");
            done();
        };
    });
});
