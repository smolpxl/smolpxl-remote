use crate::admin_message::AdminMessage;
use crate::app_state::AppState;
use crate::data_message::DataMessage;
use crate::idgen;
use crate::socket_role::{Owner, Satellite, SocketRole};
use actix::{Actor, AsyncContext, Handler, Message, StreamHandler};
use actix_web::web;
use actix_web_actors::ws;
use log::{debug, error, info};
use serde_json::json;
use std::sync::Mutex;
use std::time::Duration;

#[derive(Debug)]
pub struct SmolpxlWebsocket {
    data: web::Data<Mutex<AppState>>,
    role: SocketRole,
    expiry_time: Duration,
}

impl SmolpxlWebsocket {
    pub fn new(data: web::Data<Mutex<AppState>>) -> Self {
        // Note: No need to lock here if AppState contains Mutexes,
        // instead of being wrapped inside one.
        let expiry_time = data.lock().unwrap().expiry_time();

        Self {
            data,
            role: SocketRole::Undecided,
            expiry_time,
        }
    }

    fn handle_text(
        &mut self,
        text: String,
        ctx: &mut <SmolpxlWebsocket as Actor>::Context,
    ) {
        match &mut self.role {
            SocketRole::Undecided => self.create_or_connect(text, ctx),
            SocketRole::Owner(owner) => owner.send_to_satellites(text),
            SocketRole::Satellite(satellite) => satellite.send_to_owner(text),
        }
    }

    fn create_or_connect(
        &mut self,
        text: String,
        ctx: &mut <SmolpxlWebsocket as Actor>::Context,
    ) {
        let text = text.trim();
        if text == "new" {
            self.create_game(ctx);
        } else if text.starts_with("take ") {
            self.take_game(&text[5..], ctx);
        } else if text.starts_with("connect ") {
            self.connect_to_game(&text[8..], ctx);
        } else {
            self.error(
                ctx,
                "Expecting 'new', 'connect ID', or 'take ID SECRET'.",
            );
        }
    }

    fn create_game(&mut self, ctx: &mut <SmolpxlWebsocket as Actor>::Context) {
        let purge = {
            let id = idgen::new_id();
            let secret = idgen::new_secret();
            let mut data = self.data.lock().unwrap();
            let add_result =
                data.add_game(id.clone(), secret.clone(), ctx.address());
            if let Ok(purge_expired) = add_result {
                self.role = SocketRole::Owner(Owner::new());
                ctx.text(json!({ "id": id, "secret": secret }).to_string());
                debug!("> Game {} started.", id);
                purge_expired
            } else {
                self.error(ctx, "Sorry, too many games. Please try later.");
                true // Always purge if we have a lot of games
            }
        };

        if purge {
            self.purge_expired();
        }
    }

    fn take_game(
        &mut self,
        id_and_secret: &str,
        ctx: &mut <SmolpxlWebsocket as Actor>::Context,
    ) {
        if let Some((id, secret)) = id_and_secret.split_once(' ') {
            let data = &mut self.data.lock().unwrap();

            if let Ok(old) = data.replace_game(id, secret, ctx.address()) {
                self.role = SocketRole::Owner(Owner::new());
                // Ask old to close and give us its satellites
                old.do_send(AdminMessage::TakenBy(ctx.address()));
                ctx.text(json!({ "id": id, "secret": secret }).to_string());
                debug!("* Game {} taken by new WebSocket.", id);
            } else {
                self.error(ctx, "take failed - no such game");
            }
        } else {
            self.error(ctx, "take failed - you must supply game_id and secret");
        }
    }

    fn purge_expired(&mut self) {
        let games = self.data.lock().unwrap().games_clone();

        info!(
            "Purging expired games. (Before purge there were {} games.)",
            games.len()
        );

        for (id, game) in games {
            if game.owner.connected() {
                game.owner.do_send(AdminMessage::PurgeExpired(id));
            } else {
                debug!("< Game {} disconnected.", id);
                self.data.lock().unwrap().remove_game(&id);
            }
        }
    }

    fn connect_to_game(
        &mut self,
        game_id: &str,
        ctx: &mut <SmolpxlWebsocket as Actor>::Context,
    ) {
        let data = &mut self.data.lock().unwrap();

        if let Some(owner) = data.owner(game_id) {
            self.role = SocketRole::Satellite(Satellite::new(owner.clone()));
            if let Ok(_) =
                owner.try_send(AdminMessage::NewSatellite(ctx.address()))
            {
                debug!("+ Game {} added satellite.", game_id);
                ctx.text(json!({"connected": "ok"}).to_string());
            } else {
                self.error(ctx, "connect failed - game disconnected");
            }
        } else {
            self.error(ctx, "connect failed - no such game");
        }
    }

    fn error(&self, ctx: &mut <SmolpxlWebsocket as Actor>::Context, msg: &str) {
        ctx.text(json!({ "error": msg }).to_string());
        ctx.close(Some(ws::CloseReason {
            code: ws::CloseCode::Policy,
            description: Some(String::from(msg)),
        }));
    }
}

impl Actor for SmolpxlWebsocket {
    type Context = ws::WebsocketContext<Self>;
}

impl Handler<DataMessage> for SmolpxlWebsocket {
    type Result = <DataMessage as Message>::Result;

    fn handle(
        &mut self,
        msg: DataMessage,
        ctx: &mut <Self as Actor>::Context,
    ) -> <Self as Handler<DataMessage>>::Result {
        ctx.text(msg.text);
    }
}

impl Handler<AdminMessage> for SmolpxlWebsocket {
    type Result = <AdminMessage as Message>::Result;

    fn handle(
        &mut self,
        msg: AdminMessage,
        ctx: &mut <Self as Actor>::Context,
    ) -> Self::Result {
        match &mut self.role {
            SocketRole::Owner(owner) => match msg {
                AdminMessage::TakenBy(new_owner) => {
                    ctx.close(Some(ws::CloseReason {
                        code: ws::CloseCode::Normal,
                        description: Some(String::from(
                            "Someone else took the connection.",
                        )),
                    }));
                    // Tell our satellites about the new owner, and tell
                    // the new owner about all our satellites
                    for sat in &owner.satellites {
                        sat.do_send(AdminMessage::ChangeOwner(
                            new_owner.clone(),
                        ));
                        new_owner
                            .do_send(AdminMessage::NewSatellite(sat.clone()));
                    }
                }
                AdminMessage::NewSatellite(satellite) => {
                    owner.satellites.push(satellite);
                }
                AdminMessage::PurgeExpired(game_id) => {
                    if owner.last_changed.elapsed() >= self.expiry_time {
                        debug!("< Game {} expired.", game_id);
                        self.data.lock().unwrap().remove_game(&game_id);
                    }
                }
                _ => {
                    error!(
                        "Received unexpected AdminMessage message {:?} on an \
                        owner.",
                        msg
                    );
                }
            },
            SocketRole::Satellite(satellite) => match msg {
                AdminMessage::ChangeOwner(new_owner) => {
                    satellite.owner = new_owner;
                }
                _ => {
                    error!(
                        "Received unexpected AdminMessage message {:?} on a \
                        satellite.",
                        msg
                    );
                }
            },
            SocketRole::Undecided => {
                error!(
                    "Received AdminMessage message {:?} on an undecided!",
                    msg
                );
            }
        }
    }
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>>
    for SmolpxlWebsocket
{
    fn handle(
        &mut self,
        msg: Result<ws::Message, ws::ProtocolError>,
        ctx: &mut Self::Context,
    ) {
        match msg {
            Ok(ws::Message::Ping(msg)) => ctx.pong(&msg),
            Ok(ws::Message::Text(text)) => self.handle_text(text, ctx),
            //Ok(ws::Message::Binary(bin)) => We don't use binary,
            _ => (),
        }
    }
}
