mod admin_message;
mod app_state;
mod data_message;
mod idgen;
mod smolpxl_websocket;
mod socket_role;

use crate::app_state::AppState;
use crate::smolpxl_websocket::SmolpxlWebsocket;
use actix_web::middleware::Logger;
use actix_web::{
    middleware, web, App, Error, HttpRequest, HttpResponse, HttpServer,
};
use actix_web_actors::ws;
use log::{debug, info};
use rustls::internal::pemfile::{certs, pkcs8_private_keys};
use rustls::{NoClientAuth, ServerConfig};
use std::fs::File;
use std::io::{self, BufReader};
use std::sync::Mutex;
use std::time::Duration;

async fn index(
    req: HttpRequest,
    stream: web::Payload,
    data: web::Data<Mutex<AppState>>,
) -> Result<HttpResponse, Error> {
    let resp = ws::start(SmolpxlWebsocket::new(data), &req, stream);
    debug!(
        "Connection from {}",
        req.peer_addr()
            .map(|a| a.to_string())
            .unwrap_or_else(|| String::from("unknown"))
    );
    resp
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let bind_addr = "0.0.0.0:4433";
    let max_games = 1000; // Only this many simultaneous games allowed
    let purge_every = 50; // Every 50 new games, we delete expired ones

    // Delete game after 1 min inactive
    let expiry_time = Duration::from_secs(60);

    env_logger::init();
    let data = web::Data::new(Mutex::new(AppState::new(
        max_games,
        purge_every,
        expiry_time,
    )));

    let server = HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .wrap(middleware::Compress::default())
            .app_data(data.clone())
            .service(
                actix_files::Files::new("/static", "static")
                    .index_file("index.html"),
            )
            .route("/", web::get().to(index))
    });

    let fullchain = File::open("fullchain.pem");
    let privkey = File::open("privkey.pem");

    let server = match (fullchain, privkey) {
        (Ok(fullchain), Ok(privkey)) => {
            info!("Found cert and key files. Using TLS.");
            let config = tls_config(fullchain, privkey)?;
            server.bind_rustls(bind_addr, config)
        }
        (e1, e2) => {
            let mut err = String::new();
            if let Err(e1) = e1 {
                err += " ";
                err += &e1.to_string();
            }
            if let Err(e2) = e2 {
                err += " ";
                err += &e2.to_string();
            }
            info!(
                "Unable to open fullchain.pem and privkey.pem in current \
                directory.  Falling back to plain HTTP.{}",
                err
            );
            server.bind(bind_addr)
        }
    }?;

    server.run().await
}

fn tls_config(fullchain: File, privkey: File) -> io::Result<ServerConfig> {
    let mut config = ServerConfig::new(NoClientAuth::new());

    let fullchain = &mut BufReader::new(fullchain);
    let privkey = &mut BufReader::new(privkey);

    let cert_chain = certs(fullchain).map_err(|_| {
        io::Error::new(io::ErrorKind::Other, "Unable to read certs")
    })?;
    let mut keys = pkcs8_private_keys(privkey).map_err(|_| {
        io::Error::new(io::ErrorKind::Other, "Unable to read private key")
    })?;

    // Copied from actix example - I don't know why we remove 0
    config
        .set_single_cert(cert_chain, keys.remove(0))
        .map_err(|e| {
            io::Error::new(
                io::ErrorKind::Other,
                format!("Unable to set up TLS: {}", e.to_string()),
            )
        })?;

    Ok(config)
}
