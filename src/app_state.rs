use crate::smolpxl_websocket::SmolpxlWebsocket;
use actix::Addr;
use std::collections::HashMap;
use std::time::Duration;

#[derive(Debug, PartialEq)]
pub enum AddGameError {
    TooManyGames,
}

#[derive(Debug, PartialEq)]
pub enum ReplaceGameError {
    DoesNotExistOrIncorrectSecret,
}

#[derive(Debug)]
pub struct AppState(AppStateImpl<Addr<SmolpxlWebsocket>>);

impl AppState {
    pub fn new(
        max_games: usize,
        purge_every: usize,
        expiry_time: Duration,
    ) -> AppState {
        AppState(AppStateImpl::<Addr<SmolpxlWebsocket>>::new(
            max_games,
            purge_every,
            expiry_time,
        ))
    }

    pub fn add_game(
        &mut self,
        id: String,
        secret: String,
        owner: Addr<SmolpxlWebsocket>,
    ) -> Result<bool, AddGameError> {
        self.0.add_game(id, secret, owner)
    }

    pub fn replace_game(
        &mut self,
        id: &str,
        secret: &str,
        owner: Addr<SmolpxlWebsocket>,
    ) -> Result<Addr<SmolpxlWebsocket>, ReplaceGameError> {
        self.0.replace_game(id, secret, owner)
    }

    pub fn remove_game(&mut self, id: &str) {
        self.0.remove_game(id);
    }
    pub fn owner(&self, id: &str) -> Option<Addr<SmolpxlWebsocket>> {
        self.0.owner(id)
    }
    pub fn games_clone(&self) -> HashMap<String, Game<Addr<SmolpxlWebsocket>>> {
        self.0.games_clone()
    }
    pub fn expiry_time(&self) -> Duration {
        self.0.expiry_time
    }
}

#[derive(Clone, Debug)]
pub struct Game<T>
where
    T: Clone,
{
    secret: String,
    pub owner: T,
}

#[derive(Debug)]
struct AppStateImpl<T>
where
    T: Clone,
{
    // For performance, we could consider chashmap here instead
    // of wrapping this struct in a Mutex, but generally I think
    // the performance hot spot is not here (until we are very popular):
    // this is only used when we are creating and joining games,
    // not playing them.
    games: HashMap<String, Game<T>>,
    max_games: usize,
    adds_since_purge: usize,
    purge_every: usize,
    expiry_time: Duration,
}

impl<T> AppStateImpl<T>
where
    T: Clone,
{
    fn new(
        max_games: usize,
        purge_every: usize,
        expiry_time: Duration,
    ) -> Self {
        Self {
            games: HashMap::new(),
            max_games,
            adds_since_purge: 0,
            purge_every,
            expiry_time,
        }
    }

    fn add_game(
        &mut self,
        id: String,
        secret: String,
        owner: T,
    ) -> Result<bool, AddGameError> {
        if self.games.len() >= self.max_games {
            Err(AddGameError::TooManyGames)
        } else {
            self.games.insert(id, Game { secret, owner });
            self.adds_since_purge += 1;
            let purge_expired = self.adds_since_purge >= self.purge_every;
            if purge_expired {
                self.adds_since_purge = 0;
            }
            Ok(purge_expired)
        }
    }

    fn replace_game(
        &mut self,
        id: &str,
        secret: &str,
        owner: T,
    ) -> Result<T, ReplaceGameError> {
        if let Some(entry) = self.games.get_mut(id) {
            if entry.secret == secret {
                let mut owner = owner.clone();
                std::mem::swap(&mut entry.owner, &mut owner);
                Ok(owner)
            } else {
                Err(ReplaceGameError::DoesNotExistOrIncorrectSecret)
            }
        } else {
            Err(ReplaceGameError::DoesNotExistOrIncorrectSecret)
        }
    }

    pub fn remove_game(&mut self, id: &str) {
        self.games.remove(id);
    }

    pub fn owner(&self, id: &str) -> Option<T> {
        self.games.get(id).map(|game| game.owner.clone())
    }

    pub fn games_clone(&self) -> HashMap<String, Game<T>> {
        self.games.clone()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn adding_a_game_means_we_can_get_its_owner() {
        let mut s =
            AppStateImpl::<String>::new(10, 50, Duration::from_secs(60));
        s.add_game(
            String::from("id1"),
            String::from("secret"),
            String::from("owner"),
        )
        .expect("Adding game failed");
        let owner = s.owner("id1").expect("Getting owner failed");
        assert_eq!(owner, "owner");
    }

    #[test]
    fn we_can_replace_a_game_if_we_have_its_secret() {
        let id1 = String::from("id1");
        let secret = String::from("secret");
        let owner = String::from("owner");
        let new_owner = String::from("new_owner");

        // Given a game exists with owner "owner"
        let mut s =
            AppStateImpl::<String>::new(10, 50, Duration::from_secs(60));
        s.add_game(id1.clone(), secret.clone(), owner)
            .expect("Adding game failed");

        // When we replace the name, using the correct secret
        s.replace_game(&id1, &secret, new_owner)
            .expect("Replacing failed but it should not have");

        // Then the owner changed to the new one
        let actual_owner = s.owner("id1").expect("getting owner failed");
        assert_eq!(actual_owner, "new_owner");
    }

    #[test]
    fn we_cannot_replace_a_game_if_we_use_incorrect_secret() {
        let id1 = String::from("id1");
        let secret = String::from("secret");
        let owner = String::from("owner");
        let new_owner = String::from("new_owner");

        // Given a game exists with owner "owner"
        let mut s =
            AppStateImpl::<String>::new(10, 50, Duration::from_secs(60));
        s.add_game(id1.clone(), secret, owner)
            .expect("Adding game failed");

        // When we replace the name, using the WRONG secret
        s.replace_game(&id1, "INCORRECT", new_owner)
            .expect_err("Replacing should have failed");

        // Then the owner did not change
        let actual_owner = s.owner("id1").expect("getting owner failed");
        assert_eq!(actual_owner, "owner");
    }

    #[test]
    fn getting_a_nonexistent_games_owner_returns_none() {
        let mut s =
            AppStateImpl::<String>::new(10, 50, Duration::from_secs(60));
        s.add_game(
            String::from("id1"),
            String::from("secret"),
            String::from("owner"),
        )
        .expect("Adding game failed");
        assert!(s.owner("incorrect_id") == None);
    }

    #[test]
    fn removing_a_game_means_it_no_longer_exists() {
        let mut s =
            AppStateImpl::<String>::new(10, 50, Duration::from_secs(60));
        s.add_game(
            String::from("id1"),
            String::from("secret"),
            String::from("owner"),
        )
        .expect("Adding game failed");
        s.remove_game("id1");
        assert!(s.owner("id1") == None);
    }

    #[test]
    fn adding_too_many_games_eventually_fails() {
        // 10 games only allowed
        let mut s =
            AppStateImpl::<String>::new(10, 50, Duration::from_secs(60));

        // Add 10
        for i in 0..10 {
            s.add_game(
                format!("id{}", i),
                String::from("secret"),
                String::from("o"),
            )
            .unwrap();
        }

        // Attempt to add another - should fail
        assert!(
            s.add_game(
                String::from("id10"),
                String::from("secret"),
                String::from("o")
            )
            .unwrap_err()
                == AddGameError::TooManyGames
        );
    }

    #[test]
    fn we_purge_expired_every_few_adds() {
        // purge every 5
        let mut s =
            AppStateImpl::<String>::new(100, 5, Duration::from_secs(60));

        let purges: Vec<bool> = (0..10)
            .map(|i| {
                s.add_game(
                    format!("id{}", i),
                    String::from("secret"),
                    String::from("o"),
                )
                .unwrap()
            })
            .collect();

        // Attempt to add another - should fail
        assert_eq!(
            purges,
            [
                false, false, false, false, true, //
                false, false, false, false, true,
            ]
        );
    }
}
