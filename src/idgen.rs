use rand::{thread_rng, Rng};

/**
 * Return a new, unguessable game ID.
 */
pub fn new_id() -> String {
    base64::encode_config(
        thread_rng().gen::<[u8; 8]>(),
        base64::URL_SAFE_NO_PAD,
    )
}

/*
 * Return a new, unguessable secret.
 */
pub fn new_secret() -> String {
    // Since IDs must be unguessable to prevent gate-crashing, we can
    // use the same code for secrets as IDs.
    new_id()
}
