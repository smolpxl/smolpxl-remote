use crate::data_message::DataMessage;
use crate::smolpxl_websocket::SmolpxlWebsocket;
use actix::Addr;
use std::time::Instant;

#[derive(Debug)]
pub struct Owner {
    pub last_changed: Instant,
    pub satellites: Vec<Addr<SmolpxlWebsocket>>,
}

impl Owner {
    pub fn new() -> Self {
        Self {
            last_changed: Instant::now(),
            satellites: Vec::new(),
        }
    }

    pub fn send_to_satellites(&mut self, text: String) {
        self.last_changed = Instant::now();
        for satellite in &self.satellites {
            satellite.do_send(DataMessage::new(text.clone()));
        }
    }
}

#[derive(Debug)]
pub struct Satellite {
    pub owner: Addr<SmolpxlWebsocket>,
}

impl Satellite {
    pub fn new(owner: Addr<SmolpxlWebsocket>) -> Self {
        Self { owner }
    }

    pub fn send_to_owner(&self, text: String) {
        // TODO: add some kind of client ID to the message
        self.owner.do_send(DataMessage::new(text));
    }
}

#[derive(Debug)]
pub enum SocketRole {
    Undecided,
    Owner(Owner),
    Satellite(Satellite),
}
