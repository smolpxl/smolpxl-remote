use crate::smolpxl_websocket::SmolpxlWebsocket;
use actix::{Addr, Message};

#[derive(Debug)]
pub enum AdminMessage {
    ChangeOwner(Addr<SmolpxlWebsocket>),
    NewSatellite(Addr<SmolpxlWebsocket>),
    PurgeExpired(String),
    TakenBy(Addr<SmolpxlWebsocket>),
}

impl Message for AdminMessage {
    type Result = ();
}
