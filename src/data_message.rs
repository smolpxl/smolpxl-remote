use actix::Message;

#[derive(Debug)]
pub struct DataMessage {
    pub text: String,
}

impl DataMessage {
    pub fn new(text: String) -> Self {
        Self { text }
    }
}

impl Message for DataMessage {
    type Result = ();
}
