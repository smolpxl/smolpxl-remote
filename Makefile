all: test

test:
	cargo fmt
	cargo test

run:
	cargo fmt
	cargo run

doc:
	cargo doc --open
	rustup doc

setup:
	sudo apt install musl-tools
	rustup target add x86_64-unknown-linux-musl

deploy-on-target:
	# My script to deploy on my aarch64 machine
	cargo build --release
	rsync -r --delete static/ ~/smolpxl-remote/static/
	cp target/release/smolpxl-remote ~/smolpxl-remote/
	@echo 'Make sure your certs are in ~/smolpxl-remote'
	@echo 'tmux attach and restart RUST_LOG=debug ./smolpxl-remote'

deploy-old-x86_64:
	cargo build --target x86_64-unknown-linux-musl --release
	ssh smolpxlremote mkdir -p smolpxl-remote
	scp target/x86_64-unknown-linux-musl/release/smolpxl-remote smolpxlremote:smolpxl-remote/smolpxl-remote.new
	rsync -r --delete static/ smolpxlremote:smolpxl-remote/static/
	@echo 'Now run: `ssh smolpxlremote` and `screen -r`'
