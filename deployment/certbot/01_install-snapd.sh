sudo yum install snapd
sudo systemctl enable --now snapd.socket
sudo ln -s /var/lib/snapd/snap /snap
# Log out and in again
sudo snap install core
sudo snap refresh core
