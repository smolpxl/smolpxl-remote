sudo certbot certonly --nginx
sudo chmod 755 /etc/letsencrypt/live
sudo chmod 755 /etc/letsencrypt/archive
sudo chmod og+r /etc/letsencrypt/archive/andybalaam.uk.to/privkey17.pem
ln -s /etc/letsencrypt/live/andybalaam.uk.to/fullchain.pem ~/smolpxl-remote/fullchain.pem
ln -s /etc/letsencrypt/live/andybalaam.uk.to/privkey.pem ~/smolpxl-remote/privkey.pem
sudo certbot renew --dry-run
