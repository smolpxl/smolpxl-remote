#!/bin/sh
#FreeDNS updater script

DOMAIN="smolpxl.uk.to"
TOKEN="REDACTED"

cd /home/opc/afraid.org/

registered=$(nslookup $DOMAIN|tail -n2|grep A|sed s/[^0-9.]//g)
current=$(wget -q -O - http://checkip.dyndns.org|sed s/[^0-9.]//g)

if [ "$current" != "$registered" ]; then
    echo "DNS updated on:" > afraid-updated.txt
    date >> afraid-updated.txt
    wget --no-check-certificate -O - \
        "https://freedns.afraid.org/dynamic/update.php?${TOKEN}" \
        >> afraid-updated.txt  2>&1
else
    echo "DNS checked on:" > afraid-checked.txt
    date >> afraid-checked.txt
    echo "current=$current" >> afraid-checked.txt
    echo "registered=$registered" >> afraid-checked.txt
fi
