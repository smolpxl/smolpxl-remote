# For certbot
sudo firewall-cmd --zone=public --permanent --add-port=80/tcp
# For smolpxl-remote
sudo firewall-cmd --zone=public --permanent --add-port=4433/tcp

sudo firewall-cmd --reload

echo "You must also go to the Oracle cloud console, open your instance,"
echo "click its subnet, click its security list, and use Add Ingess Rules"
echo "to add rules for port 80 and port 4433 with source 0.0.0.0/0 and"
echo "protocol TCP."
