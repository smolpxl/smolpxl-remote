# Deployment

smolpxl-remote is deployed to an Oracle free tier cloud instance.

This directory contains scripts for:

* updating the DNS when the IP changes (which seems very rare).  The Dynamic
  DNS is provided by [afraid.org](https://freedns.afraid.org), and

* generating SSL certificates using [Certbot](https://certbot.eff.org/) for
  [LetsEncrypt](https://letsencrypt.org/).

* opening ports on the cloud instance.
